# SKA Use Cases

PlantUML, SVG, PNG, and PDF Use Case diagrams for the SKA telescopes.

The diagrams are beign created to be part of the SKA [Solution Intent](https://confluence.skatelescope.org/display/SWSI/Solution+Intent+Home).

### High-Level Solution Use Cases for AA0.5
![](SolutionUseCasesAA05.svg)

### High-Level Solution Use Cases
![](SolutionUseCases.svg)

### Detailed Solution Use Cases
![](SolutionUseCasesDetailed.svg)

## How to create the diagrams by yourself

The provided `Makefile` allows for automatic creation of the Use Case diagrams from the multiple `SolutionUseCases*.plantuml` files. The _Required software_ and _Optional software_ sections indicate the required software that Makefile to work.

You can use `make` or `make all` to create the diagrams from the source code. You can use `make clean` to remove the diagrams, and start again.

Additionally, on macOS you can use `make preview` to show the PDF files on Preview.app. You can use `make all preview` to make sure that all products are created, and then PDFs previewed on Preview.app.

## Required software

* PlantUML:
  * On macOS, can be installed with `brew install plantuml`.
  * On Debian-based Linuxes, can be installed with `apt get plantuml`.
  * On any other platform with a working Java installation: download `plantuml.jar` from https://plantuml.com/starting.
  * Alternatively, you can use the `SolutionUseCases.plantuml` on a PlantUML enabed editor, such as [Draw.IO](https://diagrams.net "Diagrams.net (aka Draw.IO)") or the online [PlantUML viewer](http://www.plantuml.com/plantuml/uml/).

## Optional software

* Inkscape:
  * On macOS, can be installed with `brew install inkscape`.
  * On Debian-based Linuxes, can be installed with `apt get inkscape`.
  * For other platforms, visit https://inkscape.org.
