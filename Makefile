# This Makefile builds all diagrams from the PlantUML source
SRCS := $(wildcard *.plantuml) # Finds all *.plantuml files in the repository
SVGS := $(SRCS:.plantuml=.svg) # Creates target *.svg files
PNGS := $(SRCS:.plantuml=.png) # Creates target *.png files
PDFS := $(SVGS:.svg=.pdf)      # Creates traget *.pdf files

# Declare targets as not file-related
.PHONY: all svgs pngs pdfs preview clean

# Make all products
all: $(SVGS) $(PNGS) $(PDFS)

# Make only SVGs
svgs: $(SVGS)

# Make only PNGs
pngs: $(PNGS)

# Make only PDFs
pdfs: $(PDFS)

# Open with Preview on macOS
preview: $(PDFS)
	open -a Preview $(PDFS)

# Rules for building products from other products
%.svg : %.plantuml
	plantuml -tsvg $<

%.png : %.svg
	inkscape --export-dpi=300 -o $@ $<

%.pdf : %.svg
	inkscape -o $@ $<

clean : 
	@rm -fv $(SVGS) $(PNGS) $(PDFS)